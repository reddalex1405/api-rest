package com.davidreyes.rest;

import com.davidreyes.rest.empleados.Capacitacion;
import com.davidreyes.rest.empleados.Empleado;
import com.davidreyes.rest.empleados.Empleados;
import com.davidreyes.rest.repositorios.EmpleadoDAO;
import com.davidreyes.rest.utils.Configuracion;
import com.davidreyes.rest.utils.EstadosPedido;
import com.davidreyes.rest.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {

    @Autowired
    private EmpleadoDAO empDao;

    @GetMapping(path = "/")
    public Empleados getEmpleados() {

        return empDao.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Object> getEmpleado(@PathVariable int id) {
        Empleado emp = empDao.getEmpleado(id);

        if (emp == null){
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(emp);
        }

    }

    @PostMapping(path = "/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp) {
        Integer id = empDao.getAllEmpleados().getListaEmpleados().size()+1;
        emp.setId(id);
        empDao.addEmpleado(emp);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp) {
        empDao.updEmpleado(emp);

        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int id, @RequestBody Empleado emp) {
        empDao.updEmpleado(id, emp);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleado(@PathVariable int id){
        String res = empDao.deleteEmpleado(id);

        if(res == "OK"){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> softupdEmpleado(@PathVariable int id, @RequestBody Map<String, Object> updates){
        empDao.softupdEmpleado(id, updates);

        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpleado(@PathVariable int id) {
        return ResponseEntity.ok().body(empDao.getCapacitacionEmpleado(id));
    }

    @Autowired
    Configuracion configuracion;

    @GetMapping("/autor")
    public String getAppAutor() {
        String modo = configuracion.getModo();
        String titulo = configuracion.getTitulo();
        return String.format("%s (%s)", titulo, "David");
    }

    @PostMapping(path = "/{id}/capacitaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addCapacitacionEmpleado(@PathVariable int id, @RequestBody Capacitacion cap) {
        if(empDao.addCapacitacion(id, cap)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path="/autor2")
    public String getCadena(@PathVariable String texto, @PathVariable String separador) {
        try {
            return Utilidades.getCadena(texto, separador);
        } catch (Exception e) {
            return "Failed!!!";
        }
    }
}


