package com.davidreyes.rest;

import com.davidreyes.rest.utils.BadSeparator;
import com.davidreyes.rest.utils.EstadosPedido;
import com.davidreyes.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testGetCadena() {
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "luz del sol";
        assertEquals(correcto, empleadosController.getCadena(origen, "."));
    }

    @Test
    public void testSeparatorGetCadena() {
        try {
            Utilidades.getCadena("David Reyes", "      ");
            fail("Se esperaba BadSeparator");
        }catch (BadSeparator bs){

        }
    }

    @Test
    public void testGetAutor() {
        assertEquals("TechU! (David)", empleadosController.getAppAutor());
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, 15, -3})
    public void testEsImpar(int number) {
        assertTrue(Utilidades.esImpar(number));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void testEsBlanco(String texto) {
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    public void testEsBlancoCompleto(String texto) {
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido ep) {
        assertTrue(Utilidades.valoresEstadoPedido(ep));
    }
}
